const getSum = (str1, str2) => {
  if(typeof str1 != "string" || typeof str2 != "string" || isNaN(str1) || isNaN(str2)){
    return false;
  }
  let result = (str1 != "" ? parseInt(str1) : 0) + (str2 != "" ? parseInt(str2) : 0);
  return result.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countPost = 0;
  let countComments = 0;
  listOfPosts.forEach(elementPost => {
    if(elementPost["author"] == authorName){
      countPost++;
    }
    
    if(elementPost["comments"] !== undefined ){
      for (let index = 0; index < elementPost["comments"].length; index++) {
        if(elementPost["comments"][index]["author"] == authorName){
          countComments++;
        }
        
      }
    }
  });
  return "Post:" + countPost + ",comments:" + countComments;
};

const tickets=(people)=> {
  let ticketPrice = 25;
  let change = 0;
  let bill = {
    25: 0,
    50: 0,
    100: 0
  };
  for(let index = 0; index < people.length; index++){
    bill[people[index]]++;
    if(people[index] != ticketPrice){
      change = people[index] - ticketPrice;
      Object.keys(bill).reverse().forEach(element => {
        while (change >= element && bill[element] > 0 && change != 0){
          bill[element]--;
          change -= element;
        }
      });
    }
    if(change > 0){
      return "NO";
    }
  };
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
